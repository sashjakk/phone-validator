# Country by Phone 

Small Spring Boot base API for country detection from provided phone number.

Notes:

* country code provider - [wiki](https://en.wikipedia.org/wiki/List_of_country_calling_codes)
* country detection is based on naive graph implementation without 3rd party libs

### Screenshots

![Success flow](https://bitbucket.org/sashjakk/phone-validator/raw/master/images/success-flow.png)

![Error flow](https://bitbucket.org/sashjakk/phone-validator/raw/master/images/error-flow.png)

### Build

`./gradlew clean build`

### Host

Host on local machine: 

`java -jar build/libs/phone-validator-0.0.1-SNAPSHOT.jar`

### Test report

From project root dir (on macOS):

`open build/reports/tests/test/index.html`



