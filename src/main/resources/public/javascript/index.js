/** Enter key code. */
var _enterKey = 13;

/** Country by Phone API url. */
var _detectCountryApiUrl = '/api/country';

/** Hide validation error list. */
function hideErrors() {
    $('#validation-errors').hide();
}

/** Hide country. */
function hideCountry() {
    $('#country-result').hide();
}

/**
 * Display country from successful response object.
 * @param response
 */
function displayCountry(response) {
    $('#country').text(response.data.country);
    $('#country-result').show();
}

/**
 * Display error messages in list from bad request.
 * @param response
 */
function showErrors(response) {

    // clean up from previous error messages
    var validationErrorList = $('#validation-error-list');
    validationErrorList.empty();

    // create error list items
    // append to validation error collection
    response.errors
        .map(function (error) {
            var item = $('<li></li>').addClass('collection-item');
            item.text(error);
            return item;
        })
        .forEach(function (item) {
            validationErrorList.append(item);
        });

    $('#validation-errors').show();
}

/** Hide empty elements from page. */
$(document).ready(function () {
    hideCountry();
    hideErrors();
});

/** Hide empty elements from page. */
$(document).ready(function () {
    hideCountry();
    hideErrors();
});

/** Handle Enter key events as POST requests to Country by Phone API. */
$(document).keypress(function (event) {
    if (event.which !== _enterKey) {
        return;
    }

    var payload = {
        phoneNumber: $('#phone-number').val()
    };

    $.ajax({
        method: 'POST',
        contentType: "application/json; charset=utf-8",
        url: _detectCountryApiUrl,
        data: JSON.stringify(payload),
        success: function (response) {
            hideErrors();
            displayCountry(response);
        },
        error: function (jqXHR) {
            console.log(jqXHR.responseJSON);

            hideCountry();
            showErrors(jqXHR.responseJSON);
        }
    })
});