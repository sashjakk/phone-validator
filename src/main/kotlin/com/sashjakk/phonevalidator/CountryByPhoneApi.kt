package com.sashjakk.phonevalidator

import org.springframework.beans.factory.annotation.Autowired
import org.springframework.http.HttpStatus.BAD_REQUEST
import org.springframework.http.HttpStatus.OK
import org.springframework.http.MediaType.APPLICATION_JSON_VALUE
import org.springframework.http.ResponseEntity
import org.springframework.validation.BindingResult
import org.springframework.web.bind.annotation.*
import java.lang.Exception
import javax.validation.Valid
import javax.validation.constraints.NotBlank
import javax.validation.constraints.Pattern
import javax.validation.constraints.Size

/** Data model for country detection by [phoneNumber]. */
data class CountryByPhoneModel(
        @get:Pattern(regexp = "^\\+(?:[0-9]●?){6,14}[0-9]$", message = "{phoneNumber.error.mustMatchE164}")
        @get:Size(min = 6, max = 14, message = "{phoneNumber.error.mustMatchDigitsAmount}")
        @get:NotBlank(message = "{phoneNumber.error.mustNotBeEmpty}")
        val phoneNumber: String,
        val country: String? = null
)

/** Response message models. */
@Suppress("MemberVisibilityCanBePrivate")
sealed class CountryByPhoneResponse(open val status: Int) {

    /** Response message model for successful request with [status] and country info mapped to [data]. */
    data class Success(
            override val status: Int,
            val data: CountryByPhoneModel
    ) : CountryByPhoneResponse(status)

    /** Response message model for failed request with [status], [reason] and validation [errors]. */
    data class Error(
            override val status: Int,
            val reason: String,
            val errors: List<String>
    ) : CountryByPhoneResponse(status)
}

/** Throwable exception for failed requests with validation [errors]. */
class CountryByPhoneException(val errors: List<String>) : Exception()

/** Country By Phone API. */
@RestController
@RequestMapping("/api", consumes = [APPLICATION_JSON_VALUE])
class CountryByPhoneController {

    /** Country detection service instance. */
    @Autowired
    private lateinit var phoneCodeRepo: CountryByPhoneService

    /** Provides all available country codes. */
    @GetMapping("/countries")
    fun countries() = phoneCodeRepo.getCountryPhoneCodes()

    /** Detects country from [payload]. */
    @PostMapping("/country")
    fun detect(
            @Valid @RequestBody payload: CountryByPhoneModel,
            binding: BindingResult
    ): CountryByPhoneResponse.Success {

        if (binding.hasErrors()) {
            val validationErrors = binding.allErrors
                    .map { it.defaultMessage ?: "Unknown validation error" }

            throw CountryByPhoneException(validationErrors)
        }

        val result = phoneCodeRepo.detectCountryFromPhone(payload.phoneNumber)
        return CountryByPhoneResponse.Success(OK.value(), payload.copy(country = result))
    }
}

@ControllerAdvice(assignableTypes = [CountryByPhoneController::class])
@RequestMapping(produces = [APPLICATION_JSON_VALUE])
class CountryByPhoneAdvice {

    @ExceptionHandler(CountryByPhoneException::class)
    fun error(exception: CountryByPhoneException): ResponseEntity<CountryByPhoneResponse> {

        val response = CountryByPhoneResponse.Error(
                BAD_REQUEST.value(),
                BAD_REQUEST.reasonPhrase,
                exception.errors
        )

        return ResponseEntity(response, BAD_REQUEST)
    }
}