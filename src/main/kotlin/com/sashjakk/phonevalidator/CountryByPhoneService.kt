package com.sashjakk.phonevalidator

import com.sashjakk.graph.Graph
import org.jsoup.Jsoup
import org.jsoup.nodes.Element
import org.slf4j.LoggerFactory
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.context.event.ContextRefreshedEvent
import org.springframework.context.event.EventListener
import org.springframework.stereotype.Component

/** Country to phone codes mapping container. */
data class CountryPhoneCode(val country: String, val phoneCodes: List<String>)

interface CountryByPhoneService {

    /** Provides collection of available [CountryPhoneCode]s. */
    fun getCountryPhoneCodes(): List<CountryPhoneCode>

    /** Detect country from provided [phoneNumber]. */
    fun detectCountryFromPhone(phoneNumber: String): String
}

@Component
class WikiService : CountryByPhoneService {

    private val logger = LoggerFactory.getLogger(WikiService::class.java)

    private val url = "https://en.wikipedia.org/wiki/List_of_country_calling_codes"

    private val tableTag = "table"
    private val headerTag = "tbody tr th"
    private val targetHeader = "Country, Territory or Service Code"

    @Autowired
    private lateinit var repo: CountryByPhoneService

    private val graph = Graph<Char, String>()

    override fun getCountryPhoneCodes(): List<CountryPhoneCode> {
        val document = Jsoup.connect(url).get()

        // find table by corresponding header value
        // process table by row
        // ignore first row - header does not contain desired values
        // process only first two columns - country name and phone codes
        return document.select(tableTag)
                .first { it.select(headerTag).text().contains(targetHeader) }
                .select("tr")
                .drop(1)
                .flatMap { it.children().take(2) }
                .chunked(2, ::makeCountryCode)
    }

    override fun detectCountryFromPhone(phoneNumber: String): String {
        val route = phoneNumber.toCharArray().toList()

        return graph.getValue(route) ?: "Unknown"
    }

    @EventListener
    fun onContextRefreshedEvent(event: ContextRefreshedEvent) {
        val phoneCodes = repo.getCountryPhoneCodes()
        initGraph(phoneCodes)

        logger.info("Country phone codes initialized from WIKI")
    }

    /**
     * Makes [CountryPhoneCode] instance from raw [Element]s in [items].
     *
     * NOTE:
     *  - [List.first] country name
     *  - [List.last] polluted collection of phone codes.
     */
    private fun makeCountryCode(items: List<Element>): CountryPhoneCode {
        val country = items.first().text()
        val rawPhoneCodes = items.last()

        // clean up children from useless tags
        rawPhoneCodes.children()
                .filter { it.tagName() in listOf("span", "sup") }
                .onEach { it.remove() }

        // split phone codes by coma
        // remove all spaces, letters and parenthesis
        val phoneCodes = rawPhoneCodes.text()
                .split(',')
                .map { it.replace("(\\s|[A-Za-z]|\\(|\\))".toRegex(), "") }
                .toList()

        return CountryPhoneCode(country, phoneCodes)
    }

    /**
     * Initializes [graph] with countries and phone codes from [countryPhoneCodes]
     *
     * NOTE:
     *  - some countries can have multiple phone codes
     */
    private fun initGraph(countryPhoneCodes: List<CountryPhoneCode>) = countryPhoneCodes
            .flatMap(::toPathAndValue)
            .onEach { graph.push(it.first, it.second) }

    /**
     * Converts countries with one or more phone codes to [graph] consumable format.
     *
     * NOTE:
     *  - data format example: [Pair.first] = listOf('+', '3', '7', '1'), [Pair.second] = "Latvia"
     */
    private fun toPathAndValue(countryPhoneCode: CountryPhoneCode) = countryPhoneCode.phoneCodes
            .map { it.toCharArray().toList() to countryPhoneCode.country }
}