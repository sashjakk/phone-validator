package com.sashjakk.phonevalidator

import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.runApplication
import org.springframework.stereotype.Controller
import org.springframework.web.bind.annotation.GetMapping

@SpringBootApplication
class PhoneValidatorApp

@Controller
class CountryPhoneController {

    @GetMapping("/")
    fun index() = "index.html"
}

fun main(args: Array<String>) {
    runApplication<PhoneValidatorApp>(*args)
}
