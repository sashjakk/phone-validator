package com.sashjakk.phonevalidator

import junitparams.JUnitParamsRunner
import junitparams.Parameters
import org.hamcrest.MatcherAssert.assertThat
import org.hamcrest.Matchers.contains
import org.hamcrest.Matchers.hasItem
import org.junit.Assert.assertEquals
import org.junit.ClassRule
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.context.SpringBootTest
import org.springframework.boot.test.web.client.TestRestTemplate
import org.springframework.core.ParameterizedTypeReference
import org.springframework.http.HttpEntity
import org.springframework.http.HttpHeaders
import org.springframework.http.HttpMethod.GET
import org.springframework.http.HttpStatus
import org.springframework.http.MediaType
import org.springframework.test.context.junit4.rules.SpringClassRule
import org.springframework.test.context.junit4.rules.SpringMethodRule

@RunWith(JUnitParamsRunner::class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
class CountryByPhoneControllerTest {

    companion object {
        @ClassRule
        @JvmField
        val SPRING_CLASS_RULE = SpringClassRule()
    }

    @Rule
    @JvmField
    final val springMethodRule = SpringMethodRule()

    // constants
    private val countriesAmount = 280

    // endpoints
    private val apiCountriesUrl = "/api/countries"
    private val apiCountryUrl = "/api/country"

    // shortcuts
    private val errorClass = CountryByPhoneResponse.Error::class.java
    private val successClass = CountryByPhoneResponse.Success::class.java

    @Autowired
    lateinit var rest: TestRestTemplate

    @Test
    fun `ensure all countries loaded`() {
        val headers = HttpHeaders().apply { contentType = MediaType.APPLICATION_JSON }
        val httpEntity = HttpEntity(null, headers)
        val responseType = object : ParameterizedTypeReference<ArrayList<CountryPhoneCode>>() {}
        val response = rest.exchange(apiCountriesUrl, GET, httpEntity, responseType)

        assertEquals(countriesAmount, response.body?.size)
    }

    @Test
    fun `bad request if empty phone code`() {
        val data = CountryByPhoneModel("")

        onBadRequest(data) {
            assertEquals(it.errors.size, 3)
            assertThat(it.errors, hasItem("Phone number can not be empty"))
        }
    }

    @Test
    fun `bad request if not enough digits in phone`() {
        val data = CountryByPhoneModel("+1234")

        onBadRequest(data) {
            assertEquals(it.errors.size, 2)
            assertThat(it.errors, hasItem("Phone number must contain from 6 up to 14 digits"))
        }
    }

    @Test
    fun `bad request if does not match international phone format`() {
        val data = CountryByPhoneModel("1234567")

        onBadRequest(data) {
            assertEquals(it.errors.size, 1)
            assertThat(it.errors, contains("Phone number must match international phone format ITU-T E.164"))
        }
    }

    @Test
    @Parameters(value = [
        "+37123456, Latvia",
        "+37023456, Lithuania",
        "+37223456, Estonia",
        // check for multiple codes for single country
        "+59933456, Caribbean Netherlands",
        "+59943456, Caribbean Netherlands"
    ])
    fun `success if country detected correctly`(phone: String, country: String) {
        val data = CountryByPhoneModel(phone)
        val response = rest.postForEntity(apiCountryUrl, data, successClass)
        val success = response.body!!

        assertEquals(success.status, HttpStatus.OK.value())

        assertEquals(country, success.data.country)
    }

    private inline fun onBadRequest(
            data: CountryByPhoneModel,
            action: (CountryByPhoneResponse.Error) -> Unit
    ) {
        val response = rest.postForEntity(apiCountryUrl, data, errorClass)

        val failure = response?.body!!

        assertEquals(failure.status, HttpStatus.BAD_REQUEST.value())
        assertEquals(failure.reason, HttpStatus.BAD_REQUEST.reasonPhrase)

        action(failure)
    }
}