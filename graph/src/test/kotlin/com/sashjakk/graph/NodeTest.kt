package com.sashjakk.graph

import org.junit.jupiter.api.Assertions
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.Test

internal class NodeTest {

    private val nonExistingKey = "non existing key"
    private val existingKey = "existing key"
    private val existingValue = "hello, node"
    private val nodeKey = "hey"

    private lateinit var node: Node<String, String>
    private val child = Node(existingKey, existingValue)

    @BeforeEach
    fun setup() {
        node = Node(nodeKey)
        node.add(child)
    }

    @Test
    fun `has correct key`() {
        Assertions.assertEquals(nodeKey, node.key)
    }

    @Test
    fun `has no value`() {
        Assertions.assertNull(node.value)
    }

    @Test
    fun `has child`() {
        Assertions.assertEquals(1, node.children.size)
    }

    @Test
    fun `provides null if no matching key`() {
        Assertions.assertNull(node[nonExistingKey])
    }

    @Test
    fun `provides child for matching key`() {
        Assertions.assertEquals(child, node[existingKey])
    }
}