package com.sashjakk.graph

import org.junit.jupiter.api.Assertions.*
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.assertThrows

internal class GraphTest {

    private val existingPath = "aa".toCharArray().toList()
    private val existingPath2 = "ab".toCharArray().toList()
    private val throwablePath = "ba".toCharArray().toList()
    private val nonExistingPath = "bb".toCharArray().toList()
    private val value = "value"

    private lateinit var graph: Graph<Char, String>

    @BeforeEach
    fun setup() {
        graph = Graph()
    }

    @Test
    fun `root is empty before push`() {
        assertNull(graph.root)
    }

    @Test
    fun `push to empty graph`() = initializedGraph {
        assertNotNull(it.root)
        assertEquals(1, it.root?.children?.size)
    }

    @Test
    fun `push to non empty graph`() = initializedGraph {
        graph.push(existingPath2, value)

        assertNotNull(it.root)
        assertEquals(2, it.root?.children?.size)
    }

    @Test
    fun `push with different root`() = initializedGraph {
        val exception = assertThrows<IllegalStateException> {
            graph.push(throwablePath, value)
        }

        assertEquals("Graph can have only one root node", exception.message)
    }

    @Test
    fun `get value for existing path`() = initializedGraph {
        val actual = graph.getValue(existingPath)
        assertEquals(value, actual)
    }

    @Test
    fun `get value for non existing path`() = initializedGraph {
        val actual = graph.getValue(nonExistingPath)
        assertNull(actual)
    }

    private inline fun initializedGraph(action: (Graph<Char, String>) -> Unit) {
        graph.push(existingPath, value)
        action(graph)
    }
}