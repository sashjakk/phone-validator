package com.sashjakk.graph

/**
 * Operation provider over [Node] and it's connections.
 */
class Graph<K : Any, V : Any?> {

    /** Main [Node] of this graph, initializes after first [push] operation. */
    private lateinit var _root: Node<K, V>

    val root: Node<K, V>? get() = if (::_root.isInitialized) _root else null

    /** Pushes [value] to _root node with provided [path]. */
    fun push(path: List<K>, value: V) {

        // determine existing nodes on provided path
        val existingNodePath = if (::_root.isInitialized) {
            walk(_root, path).takeUnless { it.isEmpty() }
                    ?: throw IllegalStateException("Graph can have only one root node")
        } else emptyList()

        // leave only non-existing path segment
        val valuePath = path.toMutableList().drop(existingNodePath.size)

        // append to last known node in provided path
        var parent = existingNodePath.lastOrNull()
        var child: Node<K, V>

        valuePath.forEachIndexed { index, key ->
            child = Node(key, if (index != valuePath.size - 1) null else value)

            if (!::_root.isInitialized) _root = child

            parent?.add(child)
            parent = child
        }
    }

    /** Walks the [path] in provided [node] and returns existing [nodePath] or [emptyList]. */
    tailrec fun walk(
            node: Node<K, V>,
            path: List<K>,
            nodePath: MutableList<Node<K, V>> = mutableListOf()
    ): List<Node<K, V>> {

        if (!::_root.isInitialized) throw IllegalStateException("Initialize the graph with push operation first")

        val key = path.firstOrNull() ?: return nodePath
        if (node.key != key) return nodePath

        nodePath += node

        val nextRoute = path.drop(1)
        val nextKey = nextRoute.firstOrNull() ?: return nodePath
        val nextNode = node[nextKey] ?: return nodePath

        return walk(nextNode, nextRoute, nodePath)
    }

    fun getValue(path: List<K>) = walk(_root, path).lastOrNull()?.value
}