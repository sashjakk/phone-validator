package com.sashjakk.graph

/**
 * Data container, which has associated [key] and optional [value],
 * with ability to link to other similar nodes.
 */
class Node<K : Any, V : Any?>(val key: K, val value: V? = null) {

    /** Node connections. */
    internal val children = mutableMapOf<K, Node<K, V>>()

    /** Adds connection to [child] node. */
    fun add(child: Node<K, V>) = children.put(child.key, child)

    /** Gets connected child node by [key] or 'null' if no connection. */
    operator fun get(key: K) = children[key]

    override fun toString() = "Node(key=$key, value=$value)"
}